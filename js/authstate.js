function Init(){
    var User;
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('user_menu');
        if (user) {
            User = user;
            WriteData(User.uid, User.email);
            menu.innerHTML = User.email ;
            console.log("User is logined", user);
            document.getElementById('logout').addEventListener('click',function(){
                firebase.auth().signOut()
                .then(function () {
                    alert("User Log out!!");
                    setTimeout('window.location.href = "index.html"' , 1000);
                })
                .catch(function (error) {
                    console.log("User sign out error!");
                });
            });
        }
        else{
            User = null;
            console.log("User is not logined yet.");
        }
    });
   
    var post_context = document.getElementById('post_context');
    var post_btn = document.getElementById('post_submit');
    post_btn.addEventListener('click',function(){
        var d = new Date();
        var loginUser = firebase.auth().currentUser;
        if(post_context.value != ""){
            var postData = {
                uid: loginUser.uid,
                email: loginUser.email,
                data: post_context.value,
                time: d.getFullYear()+"/"+ (d.getMonth()+1) +"/"+d.getDate() + " " + d.getHours() + ":" + d.getMinutes(),
                love: 0
            };
            console.log(d);
            var newPostKey = firebase.database().ref().child('posts').push().key;
            var updates = {};
            updates['/post/' + newPostKey] = postData;
            updates['/user/' + loginUser.uid+ '/' + newPostKey] = postData;
            firebase.database().ref().update(updates)
            .then(function(){
                post_context.value = "";
            });
        }
    });
    var html_front = " <div class='my-3 p-3 bg-white rounded'><h5 class='border-bottom border-gray pb-2 mb-0'><strong>";
    var html_mid = "</h5 ><div class='media text-muted pt-3'><p class='comment  pb-3 mb-0  lh-125 border-bottom border-gray'>";
    var html_back = " </p></div> <button class = 'btn-comment heart'";
    var html_heartid = "><i class = 'fas fa-heart'></i><p class='comment number'>";
    var html_button = "</p></button><button class = 'btn-comment'><i class = 'fas fa-comments'></i></button></div>";
    var postRef = firebase.database().ref('/post/');
    var postlist = [];
    var first_count = 0;
    var second_count = 0;
    //get every element in post/
    postRef.once('value')   
    .then(function(snapshot){
        var User = firebase.auth().currentUser;
        snapshot.forEach(function(childSnapshot){
            var child_data = childSnapshot.val();//child itself
            postlist.unshift(html_front + child_data.email + "</strong><span class='time'>" + child_data.time + "</span>" + 
                html_mid + child_data.data + html_back + "id ="+ childSnapshot.key+ html_heartid +
                child_data.love + html_button );
            first_count += 1;
        });
        document.getElementById('post_area').innerHTML = postlist.join("");
        postRef.on('child_added', function (snapshot) {
            var child_data = snapshot.val();
            second_count += 1;
            if (child_data.uid === User.uid && second_count > first_count) { 
                var new_post = document.createElement('div');
                new_post.innerHTML = html_front + child_data.email + "</strong><span class='time'>" + child_data.time + "</span>" +
                    html_mid + child_data.data + html_back + "id =" + snapshot.key + html_heartid +
                    child_data.love + html_button ;
                document.getElementById('post_area').insertBefore(new_post, document.getElementById('post_area').childNodes[0]);
            }
            var btn_heart = document.getElementById(snapshot.key);
            if(btn_heart){
                btn_heart.addEventListener('click', function () {
                    var Uid = User.uid;
                    var globalRef = firebase.database().ref('/post/' + snapshot.key);
                    var love_number;
                    var lover = child_data.lover;
                    globalRef.transaction(function (post) {
                        if (post.lover && post.lover[Uid]) {
                            post.love--;
                            post.lover[Uid] = null;
                        } else {
                            post.love++;
                            if (!post.lover) {
                                post.lover = {};
                            }
                            post.lover[Uid] = true;
                        }
                        love_number = post.love;
                        return post;
                    });
                    document.getElementById(snapshot.key).innerHTML = "<i class='fas fa-heart'></i> <p class='comment number'>" + love_number + "</p>";
                });
            }
        });
    });
}
function WriteData(Id,email){
    var userdata = {};
    userdata['/user/'+Id+'/email']=email;
    firebase.database().ref().update(userdata);
}
window.onload = function(){
    Init();
};
