function Init(){
    //sign in with email
    document.getElementById("login").addEventListener("click", function () {
        var email = document.getElementById("email");
        var password = document.getElementById("password");
        firebase.auth().signInWithEmailAndPassword(email.value, password.value)
        .then(function () {
            create_alert("success", "Welcome!!" + email.value);
            setTimeout('window.location.href = "user.html"', 2000);
        })
        .catch(function (error) { // Handle Errors here. 
            var errorCode = error.code;
            var errorMessage = error.message;
            create_alert("error", errorMessage);
            email.value = "";
            password.value = "";
        });
    });
    //sign in with google
    document.getElementById("google").addEventListener('click',function(){
        var provider = new firebase.auth.GoogleAuthProvider();
        firebase.auth().signInWithPopup(provider)
        .then(function(result){
            var token = result.credential.accessToken;
            var user = result.user;
            create_alert("success", "Google login successfully!!");
            setTimeout('window.location.href = "user.html"', 2000);
        })
        .catch(function(error){
            var errorCode = error.code;
            var errorMessage = error.message;
            var email = error.email;
            var credential = error.credentail;
            create_alert("error", errorMessage);
        });
    });
    document.getElementById("facebook").addEventListener('click',function(){
        var provider = new firebase.auth.FacebookAuthProvider();
        firebase.auth().signInWithPopup(provider)   
        .then(function(result){
            var token = result.credentail.accessToken;
            var user = result.user;
            create_alert("success","Facebook login successfully!!");
            setTimeout('window.location.href = "user.html"', 2000);
        })
        .catch(function(error){
            var errorCode = error.code;
            var errorMessage = error.message;
            var email = error.email;
            var credential = error.credentail;
            create_alert("error", errorMessage);
        });
    });
}
function create_alert(type, message) {
    var alertarea = document.getElementById('custom-alert');
    if (type == "success") {
        str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Congradulation! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    } else if (type == "error") {
        str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    }
}

window.onload = function(){
    Init();
};