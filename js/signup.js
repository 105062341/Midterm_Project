var submitting = document.getElementById("submit").addEventListener("click",function(){
    var email = document.getElementById("email");
    var password = document.getElementById("password");
    firebase.auth().createUserWithEmailAndPassword(email.value, password.value)
    .then(function(){
        create_alert("success", "Successfully Signed up!!");
        setTimeout('window.location.href = "user.html"', 3000);
    })
    .catch(function (error) { // Handle Errors here. 
        var errorCode = error.code; 
        var errorMessage = error.message; 
        create_alert("error", errorMessage);
        email.value = "";
        password.value = "";
    });
});
function create_alert(type, message) {
    var alertarea = document.getElementById('custom-alert');
    if (type == "success") {
        str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Congradulation! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    } else if (type == "error") {
        str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    }
}