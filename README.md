# Software Studio 2018 Spring Midterm Project

## Topic
* Forum
* Key functions (add/delete)
    1. Post page
    2. comments
    3. Postlist page
    4. user page
* Other functions (add/delete)
    1. 重設密碼
    2. 當其他用戶發文會發送通知
    3. 刪除貼文 
    4. 按讚功能

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|GitLab Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|
|Other functions|1~10%|Y|
* * *
# Website Detail Description
這次的project中，是利用firebase的後端來連結前端的網站來實作，一開始我選擇的是forum的形式為底，往FB的樣式做發展。

## **Firebase**
firebase在這次的實作中為十分重要的角色，從認證、database到storage都必須靠firebase來幫助我們完成，首要工作便是將他初始化。

`firebase.initializeApp(config) `
## **Sign In / Sign Up**
Sign In跟Sign up的功能主要是靠firebase的方式去實作

**Sign up:** 從html的input中取得email和password，便可以在利用下方的程式使firebase建立用戶
```js
firebase.auth().createUserWithEmailAndPassword(email, password).then(function(){
    //create successfully
}).catch(function(error){
    //deal with create error
})
```
**Sign In:** 同樣在html的input中取得email和password，就能利用下方的程式進行登入
```js
firebase.auth().signInWithEmailAndPassword(email,password)
.then(function(){
    //signin successfully
}).catch(function(error){
    //deal with signin error
})
```
不論是在sign in或是sign up中，都有function去承接結果，若成功便執行`then() `裡面的function；相反，若有問題便執行`catch() `的function。

## **Manage User and Sign out**
user成功登入後，必須要取得user的資料，如此才方便使用後續的其他功能，因此必須利用下面方式來取得
```js
var user = firebase.auth().currentUser;
firebase.auth().onAuthStateChanged(function(user){

})
```
上面兩種皆能取得當下的user，差別在於上面的function取得當下user時會有取得**null**的情形，原因有可能是auth還在initial的緣故。

而下面的會監聽當下的狀態，會有小延遲的產生。
為了避免取到null的情況，因此絕大部分都是使用`onAuthStateChabged `。

而取得當下user後便可以得知user的email、name和登入方式等等，也因此可方便後續功能的實作。

另外，再取得user資訊時候，便可以很快的實作出signout的部分
```js
firebase.auth().signOut()
```
## **Post page and Postlist page**
由於我這次是往FB方向實作，有別於一般的論壇，所有的post以及其內容會被顯示在同一個頁面上。

首先若想將post或是comment即時呈現的話，就要使用realtime database，
因此我將我的user資料存放在user的reference下，並將產生的post存放在另一個post得reference下，且同時讓post有user的部分資料和user有其寫過的post資料，如此可增加之後再使用的便利性。在實作中我利用updata的方式來寫進database：
```js
firebase.database().ref().update(updates)
```
接著我利用`once('value') `和`on(child_added) `來幫助post的產生，使用once可以幫助將所有post一口氣取出，而使用on便可以即時拿到新的post並呈現。
## **留言功能**
由於所有post都在同一個頁面的緣故，所以必須知道當下按下的留言鈕是屬於哪一個post，因此我需要在child_added時候對每一個留言的button都加了監聽，但是由於每個button都一樣，因此必須對每一個button都加上一個獨立的id利用（post的key），接著便可以轉寫留言並存放在database中。

另外在每次button按下後，便會對這個post的comment reference進行一次once跟on來初始化所有留言和即時更新留言。

## **按讚功能**
和留言功能相似，必須對每個按讚的button進行監聽，此外必須使用transaction的方式去動態更新後面的數字，在一開始嘗試用update但是數字不會正確跳動
```
globalRef.transaction(function (post))
```
## **刪除貼文**
刪除貼文方式是利用`remove()`進行刪除，最主要是對其他所有會影響的reference進行update。
## **userpage**
userpage中設置讓user可以自行更動大頭貼和name，name的部分只要利用`updateProfile`即可完成，而storage的部分則是把URL上傳到firebase的storage裡面，而使用方式和realtime database相似，此外也可以重設密碼
並寄信到信箱中。

另外，所以post和comment都會有大頭貼，但必須先上傳圖片才行
```
storageRef.child('user/' + user.email).put(file);
```
## **Notification**
再通知中我設置讓每有一個user發布貼文時，便會讓每個人收到通知，因此我撤掉讓user看到別人即時發布的貼文，但user還是可以馬上看到自己發布的貼文，目的是讓user看文章時不會因此跳動頁面，而comment部分不變，可以看到別人即時發布的留言
***
## Security Report (Optional)

在security部分，首先做的便是將觀看文章以及發布文章限制在只有user登入情況下才可以使用，另外再設置密碼這種安全性行為時，會讓user輸入一次密碼來重新驗證，並在成功時候寄信到信箱中。

發布貼文時候，也禁止user輸入`<script>`來進行干擾等。