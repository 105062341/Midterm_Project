function Init(){
    var User;
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('user_menu');
        if (user) {
            User = user;
            if(user.displayName === null){
                user.updateProfile({
                    displayName: user.email,
                    photoURL: "https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg"
                }).then(function(){
                    menu.innerHTML = "<img src ='" + user.photoURL +"' class ='rounded-circle mr-2' height ='30px' width = '30px'></img>" +
                        "<p class = 'namedisplay'><strong>" + user.displayName + "</strong></p>";
                });
            }else{
                menu.innerHTML = "<img src = '" + user.photoURL + "' class ='img-nav rounded-circle mr-2' height ='30px' width = '30px'></img>" +
                    "<p class = 'namedisplay'><strong>" + user.displayName + "</strong></p>";
            }
            WriteData(User.uid, User.email , User.photoURL,User.displayName);  
            console.log("User is logined", user);
            document.getElementById('logout').addEventListener('click',function(){
                firebase.auth().signOut()
                .then(function () {
                    alert("User Log out!!");
                    setTimeout('window.location.href = "index.html"' , 1000);
                })
                .catch(function (error) {
                    console.log("User sign out error!");
                });
            });
        }
        else{
            User = null;
            console.log("User is not logined yet.");
        }
    });
//Comment and Post Function
    var post_context = document.getElementById('post_context');
    var post_btn = document.getElementById('post_submit');
    post_btn.addEventListener('click',function(){
        var d = new Date();
        var loginUser = firebase.auth().currentUser;
        var bug = '<';
        var bug2 = '>';
        var hash = false;
        if (post_context.value.includes(bug) && post_context.value.includes(bug2)){
            alert("It's illegal to do that!!");
            hash = true;
        }
        if(post_context.value != "" && !hash){
            var postData = {
                uid: loginUser.uid,
                email: loginUser.email,
                data: post_context.value,
                time: d.getFullYear()+"/"+ (d.getMonth()+1) +"/"+d.getDate() + " " + d.getHours() + ":" + d.getMinutes(),
                name: loginUser.displayName,
                love: 0
            };
            console.log(d);
            var newPostKey = firebase.database().ref().child('posts').push().key;
            var updates = {};
            updates['/post/' + newPostKey] = postData;
            updates['/user/' + loginUser.uid+ '/' + newPostKey] = postData;
            firebase.database().ref().update(updates)
            .then(function(){
                post_context.value = "";
            });
        }
    });
    var html_front = " <div class='my-3 p-3 bg-white rounded'><img class='rounded-circle mr-2'height = '30px' width = '30px' id = '";
    var html_head = "'><h5 class='pb-2 mb-0'><strong>";
    var html_mid = "</h5 ><hr class = 'my-2'><div class='media text-muted pt-3'><pre class='para pb-3 mb-0  lh-125'>";
    var html_back = " </pre></div><hr class='my-1'> <button class = 'btn-comment heart'";
    var html_heartid = "><i class = 'fas fa-heart'></i><p class='comment number'>";
    var html_button = "</p></button><button class = 'btn-comment speaker'";
    var html_speakid = "><i class = 'fas fa-comments'></i><p class='comment number'>";
    var html_delete = "</p><button class='btn-comment trashcan' id ='";
    var html_final = "'><i class = 'fas fa-trash-alt'></i></button></button></div>";
    var postRef = firebase.database().ref('/post/');
    var postlist = [];
    var first_count = 0;
    var second_count = 0;
    //get every element in post/
    postRef.once('value')   
    .then(function(snapshot){
        var User = firebase.auth().currentUser;
        snapshot.forEach(function(childSnapshot){
            var child_data = childSnapshot.val();//child itself
            postlist.unshift(html_front +childSnapshot.key+"-photo"+ html_head + child_data.name + "</strong><span class='time'>" + child_data.time + "</span>" + 
                html_mid + child_data.data + html_back + "id ="+ childSnapshot.key + html_heartid +
                child_data.love + html_button + "id =" + childSnapshot.key + "-comment" + html_speakid +
                "Add your comment!!" + html_delete + childSnapshot.key + "-delete" + html_final);
            first_count += 1;
        });
        document.getElementById('post_area').innerHTML = postlist.join("");
        postRef.on('child_added', function (snapshot) {
            var child_data = snapshot.val();
            second_count += 1;
            if (second_count > first_count) { 
                //show your data
                if(child_data.uid==User.uid){
                    var new_post = document.createElement('div');
                    new_post.innerHTML = html_front + snapshot.key + "-photo"+html_head  + child_data.name + "</strong><span class='time'>" + child_data.time + "</span>" +
                        html_mid + child_data.data + html_back + "id =" + snapshot.key + html_heartid +
                        child_data.love + html_button + "id =" + snapshot.key + "-comment" + html_speakid +
                        "Add your comment!!" + html_delete + snapshot.key + "-delete" + html_final;
                    document.getElementById('post_area').insertBefore(new_post, document.getElementById('post_area').childNodes[0]);
                }
                //notify a new post
                var notifyConfig_newpost = {
                    body: child_data.name + "\u0020release a new post",
                    icon: '/../image/notify.png'
                };
                var notification_newpost = new Notification('Someone release a new post', notifyConfig_newpost);
            }
            var post_img = document.getElementById(snapshot.key + "-photo");
            if(post_img){
                var photoRef = firebase.storage().ref();
                photoRef.child('user/'+child_data.email).getDownloadURL().then(function (url) {
                    console.log(url);
                    post_img.src = url;
                });
            }
            var btn_heart = document.getElementById(snapshot.key);
            if(btn_heart){
                btn_heart.addEventListener('click', function () {
                    var Uid = User.uid;
                    var globalRef = firebase.database().ref('/post/' + snapshot.key);
                    var love_number;
                    var lover = child_data.lover;
                    globalRef.transaction(function (post) {
                        if (post.lover && post.lover[Uid]) {
                            post.love--;
                            post.lover[Uid] = null;
                        } else {
                            post.love++;
                            if (!post.lover) {
                                post.lover = {};
                            }
                            post.lover[Uid] = true;
                        }
                        love_number = post.love;
                        return post;
                    });
                    document.getElementById(snapshot.key).innerHTML = "<i class='fas fa-heart'></i> <p class='comment number'>" + love_number + "</p>";
                });
            }
            var comment_textarea0 = "<hr class = 'my-2'><div class='mt-2 bg-comment rounded'><div class='mb-1 form-group p-1'><img class='rounded-circle mb-1' height='22px' width='22px' src='";
            var comment_textarea = "'><h6 class='ml-2'><strong>Write down your comment</strong></h6><textarea id = 'comment_context";
            var comment_textarea2 = "' class='form-control comment-textarea' rows=1 style='resize:none'></textarea></div></div>";                                 
            var comment_head = "<div class='comment_area'><hr class='my-2'><div class='mt-2 bg-comment rounded'><div class='mb-1 form-group p-1'><img height='20px' width='20px' class='rounded-circle mb-1 ' id = '";
            var comment_photo = "'><h6 class='pl-2'><strong>";
            var comment_mid = "</strong></h6><br><p class = 'para rounded mb-0 pl-1' >";
            var comment_end = "</p></div></div></div>";
            var btn_comment = document.getElementById(snapshot.key+"-comment");
            var flag = false;
            var comment_origin=0;
            var comment_add=0;
            if(btn_comment){
                btn_comment.addEventListener('click',function(){
                    btn_comment.innerHTML = "<i class='fas fa-comments'></i><p class='comment number'>Push your comment</p>";
                    if(!flag){
                        flag = true;
                        var comment_context = document.createElement('div');
                        comment_context.setAttribute('class','comment_area');
                        comment_context.innerHTML = comment_textarea0 + User.photoURL + comment_textarea + "-" + snapshot.key + comment_textarea2;
                        btn_comment.parentElement.appendChild(comment_context);
                        var commentRef = firebase.database().ref('post/'+snapshot.key+'/comments');
                        var commentlist = [];
                        if(commentRef){
                            commentRef.once('value').then(function(Snapshot){
                                Snapshot.forEach(function (childSnapshot) {
                                    var child_data = childSnapshot.val(); //child itself
                                    commentlist.unshift(comment_head+childSnapshot.key+comment_photo+child_data.User+comment_mid+child_data.data+comment_end);
                                    comment_origin++;
                                });
                                var Area = document.createElement('div');
                                Area.innerHTML = commentlist.join("");
                                btn_comment.parentElement.appendChild(Area);
                                commentRef.on('child_added',function(data){
                                    var child_data = data.val();
                                    comment_add++;
                                    var new_comment = document.createElement('div');
                                    new_comment.innerHTML=comment_head +data.key+comment_photo+ child_data.User + comment_mid + child_data.data + comment_end;
                                    if(comment_add>comment_origin){
                                        console.log(btn_comment.nextElementSibling);
                                        btn_comment.parentElement.insertBefore(new_comment,btn_comment.nextSibling.nextSibling.nextSibling);
                                    }
                                    var img_comment = document.getElementById(data.key);
                                    if(img_comment){
                                        var commentPhotoRef =firebase.storage().ref();
                                        commentPhotoRef.child('user/' + child_data.email).getDownloadURL().then(function (url) {
                                            console.log(url);
                                            img_comment.src = url;
                                        });
                                    }
                                });
                            });
                        }
                    }
                    var comment_text = document.getElementById("comment_context-" + snapshot.key);
                    var bug = '<';
                    var bug2 = '>';
                    var hash_comment = false;
                    if (comment_text.value.includes(bug) && comment_text.value.includes(bug2)) {
                        alert("It's illegal to do that!!");
                        hash_comment = true;
                    }
                    if(comment_text.value != "" &&!hash_comment){
                        
                        var update_comment = {
                            postId: snapshot.key,
                            User: User.displayName,
                            email: User.email, 
                            data: comment_text.value
                        };
                        var newPostKey = firebase.database().ref().child('comment').push().key;
                        var Update = {};
                        Update['comment/'+newPostKey] = update_comment;
                        Update['/post/'+snapshot.key+'/comments/'+newPostKey] = update_comment;
                        Update['user/' + User.uid + '/' + snapshot.key + '/comments' + newPostKey] = update_comment;
                        firebase.database().ref().update(Update).then(function(){
                            comment_text.value = "";
                        });
                    }    
                });
            }
            var btn_delete = document.getElementById(snapshot.key+"-delete");
            if(btn_delete){
                var deleteRef = firebase.database().ref();
                btn_delete.addEventListener('click',function(){
                    if(User.uid===child_data.uid){
                        firebase.database().ref('post/'+snapshot.key+'/comments').once('value')
                        .then(function(comment){
                            comment.forEach(function(childcomment){
                                firebase.database().ref().child('comment/'+childcomment.key).remove();
                            });
                        });
                        deleteRef.child('user/'+User.uid+'/'+snapshot.key).remove();
                        deleteRef.child('post/'+snapshot.key).remove();
                        btn_delete.parentElement.remove();
                    }
                    else{
                        console.log("under permission");
                    }
                });
            }
        });
    });
    postRef.on('child_changed',function(snapshot){
        var child_data = snapshot.val();
        document.getElementById(snapshot.key).innerHTML = "<i class='fas fa-heart'></i> <p class='comment number'>" + child_data.love + "</p>";
    });
//Userpage function
    var btn_Userpage = document.getElementById('user');
    btn_Userpage.addEventListener('click',function(){
        window.location.href="profile.html";

    });
}
function Notify(){
    var notifyConfig = {
        body:'Welcome to the social website',
        icon: '/../image/notify.png'
    }
    if(Notification.permission === 'default' ||Notification.permission === 'undefined'||Notification.permission === 'denied'){
        Notification.requestPermission(function(permission){
            if(permission === 'granted'){
                console.log('Notify Successfully');
                var notification = new Notification('<Hello Underworld>',notifyConfig);
            }
        });
    }
}
function WriteData(Id,email,photo,name){
    var userdata = {};
    userdata['/user/'+Id+'/email']=email;
    userdata['/user/'+Id+'/name']=name;
    userdata['/user/'+Id+'/photo']=photo;
    firebase.database().ref().update(userdata);
}
window.onload = function(){
    Notify();
    Init();
};


