function UserPageInit(){
    var btn_home = document.getElementById('home');
    var upload_input = document.getElementById('upload');
    console.log(upload_input);
    btn_home.addEventListener('click',function(){
        window.location.href = "user.html";
    });
    document.getElementById('logout').addEventListener('click', function () {
        firebase.auth().signOut()
            .then(function () {
                alert("User Log out!!");
                setTimeout('window.location.href = "index.html"', 1000);
            })
            .catch(function (error) {
                console.log("User sign out error!");
            });
    });
    firebase.auth().onAuthStateChanged(function(user){
        var menu = document.getElementById('user_menu');
        console.log(user);
        menu.innerHTML = "<img src = '" + user.photoURL + "' class ='rounded-circle mr-2' height ='30px' width='30px'></img>" +
            "<p class = 'namedisplay'><strong>" + user.displayName + "</strong></p>";
        var photo = document.getElementById('user_photo');
        photo.setAttribute('src',user.photoURL);
        var name = document.getElementById('user_name');
        name.innerHTML = "<strong>User Name:&nbsp&nbsp&nbsp&nbsp" + user.displayName+"</strong>";
        var email = document.getElementById('user_email');
        email.innerHTML = "<strong>Email:&nbsp&nbsp&nbsp&nbsp"+user.email+"</strong>";
        var varify_btn = document.getElementById('password_change');
        var flag = 1;
        varify_btn.addEventListener('click', function () {
            if(flag===1){
                document.getElementById('input_div').innerHTML = "<input class='input-edit' id='input_password' type='text'></input>";
                flag = 0;
            }
            var check = document.getElementById('input_password');
            if(check && check.value !==""){
                var credential = firebase.auth.EmailAuthProvider.credential(user.email,check.value);
                user.reauthenticateWithCredential(credential).then(function(){
                    check.value = "";
                    firebase.auth().sendPasswordResetEmail(user.email).then(function () {
                        alert('The email is send. Your password reset successfully');
                        check.value = "";
                    });
                }).catch(function(error){
                    alert('Your password is not correct');
                    check.value = "";
                });
            }
        });
        upload_input.addEventListener('change', function () {
            console.log('change');
            var storageRef = firebase.storage().ref();
            var file = this.files[0];
            var uploadTask = storageRef.child('user/' + user.email).put(file);
            uploadTask.on('state_changed', function (snapshot) { 

            },function(error){

            },function () {
                var upload_URL = uploadTask.snapshot.downloadURL;
                console.log(upload_URL);
                user.updateProfile({
                    photoURL: upload_URL
                }).then(function () {
                    photo.setAttribute('src', upload_URL);
                    menu.innerHTML = "<img src = '" + upload_URL + "' class ='rounded-circle mr-2' height ='30px' width='30px'></img>" +
                        "<p class = 'namedisplay'><strong>" + user.displayName + "</strong></p>";
                });
            });
        });
        var edit = document.getElementById('name_edit');
        edit.addEventListener('click',function(){
            var name = document.getElementById('user_name');
            var editarea = document.getElementById('editarea');
            console.log(editarea);
            if(editarea && editarea !== ""){
                user.updateProfile({
                    displayName: editarea.value
                }).then(function(){
                    name.innerHTML = "<strong>User Name:&nbsp&nbsp&nbsp&nbsp" + editarea.value + "</strong>";
                    edit.innerHTML = "<i class='fas fa-pencil-alt'></i>";
                    menu.innerHTML = "<img src = '" + user.photoURL + "' class ='rounded-circle mr-2' height ='30px' width='30px'></img>" +
                        "<p class = 'namedisplay'><strong>" + editarea.value + "</strong></p>";
                });
            }
            else{
                name.innerHTML = "<input class = 'input-edit' id='editarea' type = 'text'></input>";
                edit.innerHTML = "<i class='fas fa-check-circle'></i>";
            }
        });
    });  
}
window.onload = function(){
    UserPageInit();
};